#!/usr/bin/env python
#coding=utf-8
# __auther__:keevi
#this models is check host status
import kv_db,subprocess,time,os
import get_user
import mailer
from subprocess import Popen

#from django.core.exceptions import AppRegistryNotReady
# 检查IP表内的IP是否在serverstatus表
# 如果不存在insert
# 存在就检查hostname是否有更新，

def add_status():
    p = []
    ip_list = kv_db.IP.objects.filter(status_on=True)
    for i in ip_list:
        if len(kv_db.ServerStatus.objects.filter(host=i.ip)) ==0:

            insert_status = kv_db.ServerStatus.objects.create(name=i.hostname,host=i.ip)
            insert_status.save()
        else:
            need_name = kv_db.ServerStatus.objects.filter(host=i.ip)
            for ii in need_name:

                if ii.name != i.hostname:
                    kv_db.ServerStatus.objects.filter(host=i.ip).update(name=i.hostname)
        p.append((i.ip, Popen(['ping', '-c', '3', i.ip], stdout=subprocess.PIPE)))

    return p
#检查主机状态
def check_server():
    host_status={}
    ping_err = []
    ping_status = {}
    pp = add_status()
    host_recover =[]
    #check host ping status
    while pp:
        for i, (ip, proc) in enumerate(pp):
            if proc.poll() is not None: # ping finished
                pp.remove((ip, proc))
                if proc.returncode != 0:
                    ping_err.append(ip)

                else:
                    ping_status[ip] = proc.communicate()
    #end check host ping
#update host info to db
    for host,ping_reslut in ping_status.items():
        rtt = ping_reslut[0].split('received,')[1].split('=')[1]
        ip_ok = kv_db.ServerStatus.objects.get(host=host)
        ip_ok.ping_status = rtt
        ip_ok.status = "UP"
        ip_ok.ping_count += 1
        ip_ok.last_check = time.strftime('%Y-%m-%d %H:%M:%S')
        if ip_ok.attempts > 0:
            host_recover.append(host)
            ip_ok.attempts = 0
        ip_ok.save()
#  the inspection port info Ping failed after
    if len(ping_err) >0:
        host_down = []

        for ip in ping_err:
            ip_error = kv_db.IP.objects.get(ip=ip)
            port = ip_error.port
            db = kv_db.ServerStatus.objects.get(host=ip)
            db.ping_status="DOWN"
            command = 'nc -w2 %s %s' % (ip,port)
            result = os.system(command)
            if result == 0:
                db.status="UP"
                db.last_check = time.strftime('%Y-%m-%d %H:%M:%S')
            else:
             #   db = kv_db.ServerStatus.objects.get(host=ip)
                db.status="DOWN"
                if db.attempts == 0:
                    host_down.append(ip)
                    db.down_count +=1
                    db.attempts +=1
                else:
                    db.down_count +=1
                    db.attempts +=1
            db.ping_count +=1
            db.last_check = time.strftime('%Y-%m-%d %H:%M:%S')
            db.save()

        host_status['DOWN'] = host_down
        host_status['RECOVER'] = host_recover

#check app_port status
    host_list = kv_db.ServerStatus.objects.all()
    for info in host_list:
        app = kv_db.IP.objects.filter(ip=info.host)
        up_port = kv_db.ServerStatus.objects.get(host=info.host)
        for app_port in app:
            command = 'nc -w2 %s %s ' % (info.host,app_port.app_port)
            if os.system(command) ==0:
                up_port.app_status = "UP"

            else:
                up_port.app_status = "DOWN"
            up_port.save()
    return host_status
#end app_port
#The host down and send mail
def main():
    host_status =check_server()

    for user,info in get_user.get_user().items():
#dddddddddddddddddddddddddddddddddd
        if host_status['DOWN']:
            for host in host_status['DOWN']:
                if host in info:
                    mailer.send_mail("hello user %s ,this is a letter of the downed mail" % user,

                                     "The host name %s ,IP: %s in the %s downtime " % (info[1],host,time.strftime('%Y-%m-%d %H:%M:%S')) ,

                                     info[0])

        if host_status['RECOVER']:
            for host in host_status['RECOVER']:

                if host in info:
                    mailer.send_mail("hello user %s ,this is a letter of the host to restore mail" % user,
                                     "The host name %s ,IP: %s in the %s recover " % (info[1],host,time.strftime('%Y-%m-%d %H:%M:%S')),
                                     info[0])

#send mailor ip in host_down:
