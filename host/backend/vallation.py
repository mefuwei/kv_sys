#!/usr/bin/env python
#coding=utf-8
# Copyright (C) 2003-2007  Robey Pointer <robeypointer@gmail.com>
#
# This file is part of paramiko.
#
# Paramiko is free software; you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# Paramiko is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Paramiko; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.


import base64
from binascii import hexlify
import getpass
import os
import socket
import sys
import traceback
from paramiko.py3compat import input
import config
import paramiko
import interactive
def Usage():
    print """
    \033[;32mUsage: python %s "username@hostname [ssh-password | ssh-key] My_pass login_user" \033[0m""" % sys.argv[0]
    sys.exit()

def agent_auth(transport, username):
    """
    Attempt to authenticate to the given transport using any of the private
    keys available from an SSH agent.
    """
    
    agent = paramiko.Agent()
    agent_keys = agent.get_keys()
    if len(agent_keys) == 0:
        return

    for key in agent_keys:
        print('Trying ssh-agent key %s' % hexlify(key.get_fingerprint()))
        try:
            transport.auth_publickey(username, key)
            print('... success!')
            return
        except paramiko.SSHException:
            print('... nope.')


def manual_auth(username, hostname,auth):
    if auth == 'ssh-password':
        #pw = getpass.getpass('Password for %s@%s: ' % (username, hostname))
        try:
            pw = sys.argv[3]
        except:
            print "\033[;32m **** please enter password \033[0m"
            Usage()
        try:
            t.auth_password(username, pw)
        except :
            pass

    elif auth =='ssh-key':


        key_path = sys.argv[3]
        if len(key_path)==0:
            key_path =config.key_path

        try:
            key_path = paramiko.RSAKey.from_private_key_file(key_path)
            try:
                t.auth_publickey(username, key_path)
                print('... success!')
                return
            except paramiko.SSHException:
                print('... nope.')


        except IOError:
            print "****KEY 文件没找到,联系管理员***********"
            exit(1)




# setup logging

if len(sys.argv)<4:
    Usage()
else:


    auth = sys.argv[2]

    login_user = sys.argv[4]



    hostname = sys.argv[1]
    if hostname.find('@') >= 0:
        username, hostname = hostname.split('@')
    else:
        Usage()
    if len(hostname) == 0:
        print(' \033[;32m*** Hostname required.\033[0m')
        sys.exit(1)
    port = 22
    if hostname.find(':') >= 0:
        hostname, portstr = hostname.split(':')
        port = int(portstr)


    # now connect
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((hostname, port))
    except Exception as e:
        print('*** Connect failed: ' + str(e))
        traceback.print_exc()
        sys.exit(1)

    try:

        t = paramiko.Transport(sock)
        try:
            t.start_client()
        except paramiko.SSHException:
            print('*** SSH negotiation failed.')
            sys.exit(1)

        try:
            keys = paramiko.util.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))


        except IOError:
            try:
                keys = paramiko.util.load_host_keys(os.path.expanduser('~/ssh/known_hosts'))

            except IOError:
                print('*** Unable to open host keys file')
                keys = {}

        # check server's host key -- this is important.
        key = t.get_remote_server_key()

        if hostname not in keys:
            print('*** WARNING: Unknown host key!')
        elif key.get_name() not in keys[hostname]:
            print('*** WARNING: Unknown host key!')
        elif keys[hostname][key.get_name()] != key:
            print('*** WARNING: Host key has changed!!!')

            sys.exit(1)
        else:
            print('*** Host key OK.')

        # get username
        if username == '':
            default_username = getpass.getuser()
            username = input('Username [%s]: ' % default_username)
            if len(username) == 0:
                username = default_username

        #agent_auth(t, username)
        if not t.is_authenticated():

            manual_auth(username, hostname,auth)
        if not t.is_authenticated():

            print('*** Authentication failed.')
            t.close()
            sys.exit(1)

        chan = t.open_session()
        chan.get_pty()
        chan.invoke_shell()
        print('*** Here we go!\n')

        interactive.interactive_shell(chan,login_user,hostname)
        chan.close()
        t.close()

    except Exception as e:
        print('*** Caught exception: ' + str(e.__class__) + ': ' + str(e))
        traceback.print_exc()
        try:
            t.close()
        except:
            pass
        sys.exit(1)


