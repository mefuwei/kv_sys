#!/usr/bin/env python
# coding=utf-8
#auther:keevi
import kv_db,time,os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','ddns.settings')
def get_charts_data(hostname,charts_time):
    if charts_time == 1:
        dayRange=1800
    now_time=int(time.time())
    gt_time= now_time - dayRange
    start_time = time.localtime(gt_time)
    TIME=(start_time.tm_year,start_time.tm_mon-1,start_time.tm_mday,start_time.tm_hour,start_time.tm_min)
    data = kv_db.Day.objects.filter(sys_hostname=hostname,up_time__gt=gt_time).order_by('up_time')
    get_data_dir={}
    disk=[]
    traffic=[]
    load_list=[]
    for i in data:
        load_list.append(i.sys_load)
        disk.append(i.sys_disk)
        traffic.append(i.sys_traffic)
    get_data_dir['load_avg'] = load_list

    get_data_dir['times'] = TIME

    traffic_dict ={}
    try:
        interface = eval(traffic[0]).keys()
        for i in range(len(interface)):
            traffic_dict[interface[i]] =[]
        for i in traffic:
            b=eval(i)
            dict_merge(traffic_dict,b)
        print traffic_dict['eth0']
    except:
        interface = []



def dict_merge(a,b):
    for k, v in a.items():
        if isinstance(v, list) == False:
            a[k] = [v]
        if isinstance(b[k], list) == True:
            a[k].extend(b[k])
        else:
            a[k].append(b[k])

    return a











get_charts_data('76',1)
#1421818085