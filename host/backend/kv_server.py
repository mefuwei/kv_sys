#!/usr/bin/env python
# coding=utf-8
#auther:keevi
#this is file on the run service
import config,os,sys,time
date = time.strftime('%Y-%m-%d')
httplog = config.log_dir + '/' + 'httplog-'+ date
checklog = config.log_dir + '/' + 'checklog-'+ date
def httpserver():
    cmd = 'nohup python %s/manage.py runserver 0.0.0.0:8000 >>%s 2>&1 &' % (config.Working_dir,httplog)
    os.system(cmd)
def status_check():
    cmd = 'nohup python ./checksrv.py >>%s 2>&1 &' % (checklog)
    os.system(cmd)

def run_shell_box():

    cmd ='%s/shellbox/bin/shellinaboxd -t -b -p %s --css=%s/shellbox/share/doc/shellinabox/white-on-black.css'%(config.Working_dir,'4200',config.Working_dir)


    os.system(cmd)

try:
    if sys.argv[1] == 'start':
        httpserver()
        status_check()
        run_shell_box()



except IndexError:
    print 'No argument detected!\nUse: stop|start|status'
