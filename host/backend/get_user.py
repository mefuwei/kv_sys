#!/usr/bin/env python
# coding=utf-8
#__auther__:keevi
#The file access IP and user
import kv_db
def get_user():
    user_info = {}

    user_list = kv_db.AdminUser.objects.all()

    for info in user_list:

        user_info[info.user] = [info.email]
        for g in info.group.all():
            user_info[info.user].append(g.name)
            for ip in g.ip_set.all():
                user_info[info.user].append(ip.ip)
    return user_info
