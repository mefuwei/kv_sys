from django.contrib import admin
from host.models import *
# Register your models here.
admin.site.register(IP)
admin.site.register(Idc)
admin.site.register(Group)
admin.site.register(ServerStatus)
admin.site.register(RemoteUser)
admin.site.register(AdminUser)
admin.site.register(IpAndRemoteUser)
admin.site.register(Day)