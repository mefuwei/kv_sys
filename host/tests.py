#!/usr/bin/env python
# coding=utf-8
#auther:keevi
# -*- coding: utf-8 -*-

def dict_merge(a, b):
    for k, v in a.items():
        if isinstance(v, list) == False:
            a[k] = [v]
        if isinstance(b[k], list) == True:
            a[k].extend(b[k])
        else:
            a[k].append(b[k])
    return a

if __name__ == '__main__':
    a = {'eth1': (0.0, 0.0), 'eth0': (0.02, 0.69)}
#     b = {'eth1': (0.0, 0.0),'eth0': (0.13, 0.66)}
    b = {'eth1':[(0.0, 0.0),(0.0, 0.0)],'eth0':[(0.02, 0.69),(0.13, 0.66)]}
    print(dict_merge(a, b))
