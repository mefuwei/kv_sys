#!coding=utf-8
from django.shortcuts import render_to_response,HttpResponseRedirect,HttpResponse
from django.contrib import auth
from django.contrib.auth.decorators import login_required
import json,time
from models import *
from backend import config

# Create your views here.
def dict_merge(a,b):
    for k, v in a.items():
        if isinstance(v, list) == False:
            a[k] = [v]
        if isinstance(b[k], list) == True:
            a[k].extend(b[k])
        else:
            a[k].append(b[k])

    return a

#登陆函数

def login(req):
    
    return render_to_response('login.html')
#退出开始
def logout_view(request):
    user = request.user
    auth.logout(request)
    return HttpResponse("%s Logged out" % user)
#登陆用户验证
def account_auth(request):
    username = request.POST.get('username')

    password = request.POST.get('password')
    my_user = auth.authenticate(username=username,password=password)
    if my_user is not None:
        auth.login(request,my_user)
        return HttpResponseRedirect('/show_status/')
    else:
        return render_to_response('login.html',{'login_error':"Wrong username or password! "})
#首页
@login_required
def index(req):
    host_status_dic = {}
    for group_name in Group.objects.all():
        ip_list = IP.objects.filter(group__name=group_name)
        server_status_list = []
        for host in ip_list:
            try:
                host_status = ServerStatus.objects.get(name=host.hostname)
                #print host_status
            except:
                continue
            server_status_list.append(host_status)
        host_status_dic[group_name] = server_status_list

    return render_to_response('index.html',{'user':req.user,'host_status':host_status_dic})
#console页面组列表
@login_required
def console(req):
    login_user = req.user
    try:
        group_list = AdminUser.objects.get(user__username=login_user).group.values('id','name')

    except AdminUser.DoesNotExist:
        group_list =[]

    group_host_dic = {}
    for g in group_list:
        group_host_dic[g['id']]=g['name']
    # for need in Group.objects.all():
    #
    #     group_host_dic[need.id] = need.name
    # print group_host_dic
    return render_to_response('console.html',{'user':req.user,'group_host':group_host_dic})
@login_required()
def host(req,id):
    id = id


    host_list_dic = {}
    group_name = Group.objects.get(id=id)
    # print group_name
    data = IP.objects.filter(group__name=group_name)
    for i in data:
        host_list_dic[i.hostname] = i.ip
    res = json.dumps(host_list_dic)
    return HttpResponse(res,mimetype='application/json',)

def logout(req):
    user=req.user
    auth.logout(req)
    return render_to_response('login.html')
@login_required()
def connect(req):
    hostname = req.GET.get('hostname')
    user = req.user
    data = hostname.encode('UTF-8')
    #########################
    outfile = config.host_name
    f = open(outfile,'wb')
    f.write(data)
    f.close()
    ########################################333

    aa=json.dumps({'url':"http://192.168.1.116:4200"})
    return HttpResponse (aa,mimetype='application/json',)
def test():
    try:
        group_list = Group.objects.all()
    except AdminUser.DoesNotExist:
        group_list =[]
    group_host_dic = {}

    for g in group_list:
        group_host_dic[g.id]=g.name
    return group_host_dic
@login_required()
def charts(req):
    login_user = req.user
    group_host_dic=test()
    return render_to_response('charts.html',{'user':req.user,'group_host':group_host_dic})

def charts_region(req,id):
    group_host_dic = test()
    id=id
    name = Group.objects.get(id=id)
    ip_list = IP.objects.filter(group__name=name)
    return render_to_response('charts.html',{'user':req.user,'group_host':group_host_dic,'ip_list':ip_list})

def charts_info(req,hostname):
    data=json.dumps({'data':[['2015:01:15 10:00',1],['2015:01:15 10:05',2],['2015:01:15 10:10',0.5]]})
    return render_to_response('server.html',{'hostname':hostname})
    #return HttpResponse(data,mimetype='application/json')
#监控数据获取
def get_charts_data(hostname,charts_time):
    if charts_time == 1:
        dayRange=1800
    now_time=int(time.time())
    gt_time= now_time - dayRange
    start_time = time.localtime(gt_time)
    TIME=(start_time.tm_year,start_time.tm_mon-1,start_time.tm_mday,start_time.tm_hour,start_time.tm_min)
    data = Day.objects.filter(sys_hostname=hostname,up_time__gt=gt_time).order_by('up_time')
    get_data_dir={}
    disk=[]
    load_list=[]
    traffic=[]
    for i in data:
        load_list.append(i.sys_load)
        disk.append(i.sys_disk)
        traffic.append(i.sys_traffic)
    get_data_dir['load_avg'] = load_list

    get_data_dir['times'] = TIME
    #流量
    traffic_dict ={}
    try:
        interface = eval(traffic[0]).keys()
        for i in range(len(interface)):
            traffic_dict[interface[i]] =[]
        for i in traffic:
            b=eval(i)
            dict_merge(traffic_dict,b)

    except:
        interface = []

    d ={}
    for eth,v in traffic_dict.items():
        send = []
        rev =[]
        for (out,into) in v:
            send.append(round(out,2))
            rev.append(round(into,2))
        d[eth] = [send,rev]
    dd = dict(get_data_dir,**d)
    return dd
#监控页面返回
@login_required()
def monitor(req,hostname,charts_time=1):

    user = req.user
    if req.is_ajax():
        data=json.dumps(get_charts_data(hostname,charts_time))
        return HttpResponse(data,mimetype='application/json')
    return render_to_response('monitor.html',{'hostname':hostname,'user':user})

