from django.db import models
from django import forms
from datetime import datetime
from django.contrib.auth.models import User

class Idc(models.Model):
    name=models.CharField(max_length=50,unique=True)
    def __unicode__(self):
        return self.name

class Group(models.Model):
    name = models.CharField(max_length=50,unique=True)
    def __unicode__(self):
        return self.name

class IP(models.Model):
    hostname=models.CharField(max_length=50, unique=True)
    ip = models.IPAddressField(unique=True)
    idc = models.ForeignKey(Idc, null=True, blank=True)
    group = models.ManyToManyField(Group, null=True, blank=True)
    port = models.IntegerField(default='22')
    app_port = models.IntegerField(default='80')
    os = models.CharField(max_length=20, default='linux', verbose_name='System')
    #snmp related
    asset_collection = models.BooleanField(default=True,verbose_name='enable asset')
    status_on = models.BooleanField(default=True)
    snmp_on = models.BooleanField(default=True)
    system_load_warning = models.IntegerField(default=0,blank=True,verbose_name="load >")
    system_load_critical = models.IntegerField(default=0,blank=True)
    cpu_idle_warning = models.IntegerField(default=0,blank=True, verbose_name = "cpuIdle% < ")
    cpu_idle_critical= models.IntegerField(default=0,blank=True)
    mem_usage_warning = models.IntegerField(default=0,blank=True, verbose_name="memoryUsage% >")
    mem_usage_critical = models.IntegerField(default=0,blank=True)


    def __unicode__(self):
        return self.ip

class ServerStatus(models.Model):
    name = models.CharField(max_length=50,unique=True)
    host = models.IPAddressField()
    status = models.CharField(max_length=20,default='N/A')
    ping_status = models.CharField(max_length=100,default='unknown')
    app_status = models.CharField(max_length=20,default='unknown')
    ping_count = models.PositiveIntegerField(default=0)
    down_count = models.PositiveIntegerField(default=0)
    attempts = models.PositiveIntegerField(default=0)
    last_check = models.CharField(max_length=100,default='unknown')
    def __unicode__(self):
        return self.name
class RemoteUser(models.Model):
    username = models.CharField(max_length=50,unique=True)
    def __unicode__(self):
        return self.username
class AdminUser(models.Model):
    user = models.ForeignKey(User)
    email = models.EmailField()
    status_monitor = models.BooleanField(default=True,verbose_name='User can view host status')
    control = models.BooleanField(default=False,verbose_name='Users can perform the host command')
    remoteuser = models.ManyToManyField(RemoteUser,null=True,blank=True)
    group = models.ManyToManyField(Group, null=True, blank=True)
    host = models.ManyToManyField(IP,null=True,blank=True)
    def __unicode__(self):
        return "%s" % (self.user)

class IpAndRemoteUser(models.Model):
    ip = models.ForeignKey(IP,null=True,blank=True)
    remote_user = models.ForeignKey(RemoteUser,null=True,blank=True)
    auth_choices = (('ssh-password','SSH-PASSWORD'),('ssh-key','SSH-KEY'))
    auth_type = models.CharField(max_length=100,choices=auth_choices)
    password = models.CharField(max_length=1024,verbose_name='Password or ssh_key')

    def __unicode__(self):
        return '%s\t%s' % (self.ip,self.remote_user)
    class Meta:
        unique_together = (('ip','remote_user'),)

#charts data
class Day(models.Model):
    sys_ip = models.IPAddressField()
    sys_hostname = models.CharField(max_length=100)
    sys_load = models.FloatField()
    sys_disk = models.CharField(max_length=100)
    sys_traffic = models.TextField()
    up_time = models.PositiveIntegerField()
    def __unicode__(self):
        return self.sys_ip
class Week(models.Model):
    sys_ip = models.IPAddressField()
    sys_hostname = models.CharField(max_length=100)
    sys_load = models.FloatField()
    sys_disk = models.CharField(max_length=100)
    sys_traffic = models.TextField()
    up_time = models.PositiveIntegerField()
    def __unicode__(self):
        return self.sys_ip
class Month(models.Model):
    sys_ip = models.IPAddressField()
    sys_hostname = models.CharField(max_length=100)
    sys_load = models.FloatField()
    sys_disk = models.CharField(max_length=100)
    sys_traffic = models.TextField()
    sys_month = models.PositiveIntegerField()
    up_time = models.PositiveIntegerField()
    def __unicode__(self):
        return self.sys_ip
class Year(models.Model):
    sys_ip = models.IPAddressField()
    sys_hostname = models.CharField(max_length=100)
    sys_load = models.FloatField()
    sys_disk = models.CharField(max_length=100)
    sys_traffic = models.TextField()
    sys_year = models.PositiveIntegerField()
    up_time = models.PositiveIntegerField()
    def __unicode__(self):
        return self.sys_ip

#end


