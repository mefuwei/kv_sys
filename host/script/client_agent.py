#!/usr/bin/env python
# coding=utf-8
'''
__author__ = 'Keevi'
'''
import os,sys,time,httplib,datetime,pickle
import urllib


#配置信息
SERVER='192.168.1.116'
PORT='8000'
#参数检查
if len(sys.argv) < 2:
    print """Usage:
python %s <api_key>  OR
python %s <api_key> <ipaddress> <hostname>
but api_key is needed,""" % (sys.argv[0],sys.argv[0])
    sys.exit(0)

api_key = sys.argv[1]
# 获取IP地址，主机等信息
try:
    IP_ADDRS = sys.argv[2]
except IndexError:
    IP_ADDRS = os.popen("LC_ALL=en /sbin/ifconfig | grep 'inet addr' | grep -v '255.0.0.0' | head -n1 | cut -f2 -d':' | awk '{print $1}'").readlines()[0].strip()
try:
    HOSTNAME = sys.argv[3]

except IndexError:
    HOSTNAME = os.popen('hostname -s').readlines()[0].strip()

#获取系统负载
def get_avg_load():
  #try
  LOAD = os.popen("cat /proc/loadavg | awk '{print $1}'").readlines()[0]
  load = LOAD.strip()
  return load


#收集硬盘信息
def get_disk():
    disk_use = []
    metric_disks=os.popen('df -x tmpfs -x devtmpfs | grep -Eo " /\S*$" ').readlines()
    for disk in metric_disks:
        cmd = 'df|grep -E "%s$"' % disk.strip()
        disks = os.popen(cmd).readlines()[0]
        disk_list = disks.split()
        disk_use.append((disk_list[5],disk_list[4]))
    # print disk_use
    return disk_use

#获取网卡流量信息


def net_io_counters():

    """Return network I/O statistics for every network interface
    installed on the system as a dict of raw tuples.
    """
    f = open("/proc/net/dev", "rt")
    time_new = time.mktime(datetime.datetime.now().timetuple())
    try:
        lines = f.readlines()
    finally:
        f.close()

    retdict = {}
    for line in lines[2:]:
        colon = line.rfind(':')
        assert colon > 0, repr(line)
        name = line[:colon].strip()
        fields = line[colon + 1:].strip().split()
        bytes_recv = int(fields[0])
        bytes_sent = int(fields[8])
        if name != "lo":
            retdict[name] = (bytes_sent, bytes_recv,time_new)

    file= open('traffic.txt','wb')
    pickle.dump(retdict,file)
    file.close()
    return retdict
#收集网卡10秒内的平均流量,单位kb
def get_traffic(times=10):
    try:
        files = open('traffic.txt','rb')
        net_old = pickle.load(files)
        files.close()
    except:
        net_io_counters()
        print "初始化成功"
        exit(0)
    old_time = net_old.values()[0][2]
    net_new = net_io_counters()
    now_time = net_new.values()[0][2]
    avg_time = now_time - old_time
    traffic_list = []
    for eth,traffic in net_new.items():
        new_sent = traffic[0]
        new_recv = traffic[1]
        old_sent = net_old[eth][0]
        old_recv = net_old[eth][1]
        need_sent = float((new_sent - old_sent))/avg_time/1024
        need_recv = float((new_recv - old_recv))/avg_time/1024
        traffic_list.append((eth,need_sent,need_recv))
    return traffic_list

#发送数据
def send_data(data):

    url = '/handle_info/?ip=' + IP_ADDRS + "&hostname=" + HOSTNAME + "&keys=" + api_key

    data = urllib.urlencode(data)
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "handle_info/"}
    try:
        conn = httplib.HTTPConnection(SERVER,PORT)
        conn.request('POST',url, data,headers)
        httpres = conn.getresponse()
        print httpres.read()
    except Exception, e:
        print e
    finally:
        if conn:
            conn.close()
#收集服务器所有数据


if __name__ == "__main__":
    while True:
        server_info = {}
        server_info['load'] = get_avg_load()
        server_info['disk'] = get_disk()
        server_info['traffic'] = get_traffic()
        send_data(server_info)
        time.sleep(30)


