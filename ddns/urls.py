from django.conf.urls import patterns, include, url
from host.views import *
from host.backend.handle import *
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ddns.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    #(r'^grappelli/',include('grappelli.urls')),
    url(r'^admins/', include(admin.site.urls)),
    ('^$',login),
    ('^accounts/login/$',login),
    ('^logout/$',logout),
    ('^index/$',account_auth),
    ('^show_status/$',index),
    ('^console/$',console),
    ('^host/(\d+)$',host),
    ('^connect/$',connect),
    ('^handle_info/$',handle_info),
    ('^charts/$',charts),
    ('^charts/region/(\d+)$',charts_region),
    url(r'^charts/(?P<hostname>\S+)/detail/$',charts_info,name='status_detail'),
    url(r'^charts/(?P<hostname>\S+)/monitor/$',monitor),

)
